package stopwatch;

/**
 * Append to String test for speed test.
 * @author Napong Dungduangsasitorn
 *
 */
public class CharToString implements Runnable{

	private static final char Char = 'a';
	/** number of times that string is append.*/
	private int counter;

	/**
	 * Constructor for this test.
	 * @param counter number of times that String append.
	 */
	public CharToString(int counter){
		this.counter = counter;
	}

	/**
	 * run the test.
	 */
	@Override
	public void run() {
		String string = "";
		for(int i = 0 ; i < counter ; i++){
			string += Char;
		}
	}

	/**
	 * get test infomation.
	 * @return test information
	 */
	public String toString(){
		return String.format("append characters to a String with count = %d", counter);
	}
}
