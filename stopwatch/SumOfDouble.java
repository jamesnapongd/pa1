package stopwatch;
/**
 * Double summation test for speed test.
 * @author Napong dungduangsasitorn.
 *
 */
public class SumOfDouble implements Runnable{

	/** double array of summation. */
	private double[] sum ;
	/** number of times of the summation. */
	private int counter ;
	
	/**
	 * constructor.
	 * @param counter number of times of the summation
	 * @param arraySize size array that will be used in the test
	 */
	public SumOfDouble (int counter, int arraySize){
		this.counter = counter;
		this.sum = new double[arraySize];
		for(int i = 0 ; i < arraySize ; i++){
			sum [i] = i + 1;
		}
	}
	
	/**
	 * run
	 */
	@Override
	public void run() {
		double temp = 0;
		for(int i = 0, count = 0; count < sum.length; i++,count++){
			if(i >= sum.length) i = 0;
			temp += sum[i];
		}
	}
	
	/**
	 * get test information.
	 * @return test information
	 */
	public String toString(){
		return String.format("sum of double value from an array with count = %d and array size = %d",counter,sum.length);
	}

}
