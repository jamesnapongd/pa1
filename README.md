Stopwatch by Napong dungduangsasitorn (5710546216)

I ran the tasks on a Microsoft Surface 3 (of course), and got these results:

Task                                                                         | Time
-----------------------------------------------------------------------------|-------:
1.Append 100,000 chars to String                                             | 4.291680 sec
2.Append 100,000 chars to StringBuilder                                      | 0.003168 sec
3.Sum 100,000,000 double values from an array                                | 0.004467 sec  
4.Sum 100,000,000 Double objects having the same values as in task 3         | 0.008372 sec 
5.Sum 100,000,000 BigDecimal objects having the same values as in task 3.    | 1.708730 sec 

Explanation of Results
Reason why append char to String and StringBuilder is more in time because 
java need to convert String to StringBuilder and convert StringBuilder to String again.

For the summation test, sum 100,000,000 double values from an array shortest because 
it is primitive data. 
